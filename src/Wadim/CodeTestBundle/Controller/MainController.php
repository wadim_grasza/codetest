<?php

namespace Wadim\CodeTestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    public function showLogAction()
    {
        $varnish_log_aggregator = $this->get('varnish_log_aggregator');

        return $this->render('WadimCodeTestBundle:Default:log.html.twig', 
        array(
            "aggregates" => $varnish_log_aggregator->getAggregates(),
        ));
    }

    public function showRSSAction()
    {
        $rss_feed = $this->get('rss_feed');
        
        return $this->render('WadimCodeTestBundle:Default:rss.html.twig', 
        array(
            "items" => $rss_feed->getItems(),
        ));
    }

    public function showJSONAction()
    {
        $json_feed = $this->get('json_feed');

        return $this->render('WadimCodeTestBundle:Default:json.html.twig', 
        array(
            "items" => $json_feed->getItems(),
        ));
    }
}
