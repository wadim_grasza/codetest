<?php

namespace Wadim\CodeTestBundle\Data;

use Zend\Feed\Reader\Reader as RssReader;
use IntlDateFormatter;

/**
 *
 */
class RssFeed extends Feed {

    /**
     * Read the RSS feed from the given URI
     *
     * @param  string $etag OPTIONAL Last received ETag for this resource
     * @param  string $lastModified OPTIONAL Last-Modified value for this resource
     * @return string
     */
    public function readFeed($etag = null, $lastModified = null)
    {
        RssReader::setCache($this->getCache());
        RssReader::setHttpClient($this->getHttpClient());
        RssReader::useHttpConditionalGet(true);

        return RssReader::import($this->uri);
    }

    /**
     * Read the items from the feed to the internal items array
     *
     */
    protected function prepareItems()
    {
        $rss = $this->readFeed();

        $fmt = new IntlDateFormatter(
            'nb_NO',
            IntlDateFormatter::LONG,
            IntlDateFormatter::SHORT
        );

        $timestampedItems = array();
        foreach ($rss as $item) {
            $date = $item->getDateModified();
            if (!is_null($date)) {
                $item->dateString = $fmt->format($date);
                $timestamp = $date->getTimestamp();
            } else {
                $item->dateString = '';
                $timestamp = 0;
            }
            $timestampedItem = new \stdClass();
            $timestampedItem->timestamp = $timestamp;
            $timestampedItem->item = $item;
            $timestampedItems[] = $timestampedItem;
        }

        usort($timestampedItems, function ($a, $b) {
            return $b->timestamp - $a->timestamp;
        });

        $this->items = array_map(
            function ($timestampedItem) {
                return $timestampedItem->item;
            },
            $timestampedItems
        );
    }
}
