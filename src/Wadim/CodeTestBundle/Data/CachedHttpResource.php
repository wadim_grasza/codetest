<?php

namespace Wadim\CodeTestBundle\Data;

use Zend\Cache\Storage\StorageInterface as CacheStorage;
use Zend\Http as ZendHttp;

/**
 *
 */
class CachedHttpResource {

    /**
     * Cache to use
     *
     * @var CacheStorage
     */
    protected $cache = null;

    /**
     * HTTP client object to use for retrieving the resource
     *
     * @var Zend\Http
     */
    protected $httpClient = null;

    /**
     * The URI to the resource
     *
     * @var string
     */
    protected $uri = '';

    /**
     * Constructor
     *
     * @param  string $uri The URI to the resource
     */
    public function __construct($uri)
    {
        $this->uri = $uri;
    }

    /**
     * Get cache used by this instance
     *
     * @return  Zend\Cache\Storage\StorageInterface
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * Set cache to be used by this instance
     *
     * @param  Zend\Cache\Storage\StorageInterface $cache
     * @return void
     */
    public function setCache(CacheStorage $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Set the HTTP client instance
     *
     * Sets the HTTP client object to use for retrieving the resource
     *
     * @param  ZendHttp\Client $httpClient
     * @return void
     */
    public function setHttpClient(ZendHttp\Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }


    /**
     * Gets the HTTP client object.
     *
     * @return ZendHttp\Client
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * Read the resource from the given URI
     *
     * @param  string $etag OPTIONAL Last received ETag for this resource
     * @param  string $lastModified OPTIONAL Last-Modified value for this resource
     * @return string
     */
    public function read($etag = null, $lastModified = null)
    {
        $contents = '';
        $uri = $this->uri;
        $cache = self::getCache();
        $client = self::getHttpClient();
        $client->resetParameters();
        $headers = new ZendHttp\Headers();
        $client->setHeaders($headers);
        $client->setUri($uri);
        $cacheId = 'Wadim_CodeTestBundle_Data_CachedHttpResource_' . md5($uri);

        if ($client && $cache) {
            $data = $cache->getItem($cacheId);
            if ($data) {
                if ($etag === null) {
                    $etag = $cache->getItem($cacheId . '_etag');
                }
                if ($lastModified === null) {
                    $lastModified = $cache->getItem($cacheId . '_lastmodified');
                }
                if ($etag) {
                    $headers->addHeaderLine('If-None-Match', $etag);
                }
                if ($lastModified) {
                    $headers->addHeaderLine('If-Modified-Since', $lastModified);
                }
            }
            $response = $client->send();
            if ($response->getStatusCode() !== 200 && $response->getStatusCode() !== 304) {
                throw new RuntimeException('Resource failed to load, got response code ' . $response->getStatusCode());
            }
            if ($response->getStatusCode() == 304) {
                $contents = $data;
            } else {
                $contents = $response->getBody();
                $cache->setItem($cacheId, $contents);
                if ($response->getHeaders()->get('ETag')) {
                    $cache->setItem($cacheId . '_etag', $response->getHeaders()->get('ETag')->getFieldValue());
                }
                if ($response->getHeaders()->get('Last-Modified')) {
                    $cache->setItem($cacheId . '_lastmodified', $response->getHeaders()->get('Last-Modified')->getFieldValue());
                }
            }
            return $contents;
        } elseif ($client) {
            $response = $client->send();
            if ((int) $response->getStatusCode() !== 200) {
                throw new RuntimeException('Resource failed to load, got response code ' . $response->getStatusCode());
            }
            return $response->getBody();

        } else {
            $contents = file_get_contents($this->uri);
            if (false === $contents) {
                throw new RuntimeException('Resource failed to load');
            }
            return $contents;
        }
    }
}
