<?php

namespace Wadim\CodeTestBundle\Data;

use Zend\Json\Json;
use IntlDateFormatter;

/**
 *
 */
class JsonFeed extends Feed {

    /**
     * Read the items from the feed to the internal items array
     *
     */
    protected function prepareItems()
    {
        $json = $this->read();
        $jsonItems = Json::decode($json, Json::TYPE_ARRAY);

        $fmt = new IntlDateFormatter(
            'nb_NO',
            IntlDateFormatter::LONG,
            IntlDateFormatter::SHORT
        );
        static $jsonFeedItemDefaults = array (
            'title' => '',
            'link' => '',
            'date' => '',
            'time' => '',
            'category' => '',
            'description' => '',
        );

        $items = array();
        foreach ($jsonItems as $jsonItem) {
            $item = $jsonItem + $jsonFeedItemDefaults;

            $dateString = $item['time'].' '.$item['date'];
            $date = $fmt->parse($dateString);
            $item['dateString'] = $fmt->format($date);
            $item['timestamp'] = $date ? $date : 0;

            $items[] = $item;
        }

        usort($items, function ($a, $b) {
            return $b['timestamp'] - $a['timestamp'];
        });

        $this->items = $items;
    }
}
