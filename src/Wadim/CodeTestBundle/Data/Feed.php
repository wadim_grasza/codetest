<?php

namespace Wadim\CodeTestBundle\Data;

/**
 *
 */
abstract class Feed extends CachedHttpResource {

    /**
     * The items of this feed
     *
     * @var array
     */
    protected $items;

    /**
     * Get the feed items
     *
     * @return  array
     */
    public function getItems()
    {
        if (isset($this->items)) {
            return $this->items;
        }

        $this->prepareItems();

        return $this->items;
    }

    /**
     * Read the items from the feed to the internal items array
     *
     */
    abstract protected function prepareItems();
}
