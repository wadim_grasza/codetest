<?php

namespace Wadim\CodeTestBundle\Data;

/**
 *
 */
class VarnishLog extends CachedHttpResource {

    const DEFAULT_REQUEST_FORMAT = '/^(?:(?:(?P<method>[A-Z]+) +(?:(?P<protocol>[a-z]+):\/\/)?(?=[^ ])(?P<domain>(?:\[[0-9:]\]|[^\/?: ]+))(?::(?P<port>[0-9]+))?(?P<path>\/[^? ]*)?(?:\?(?P<querystring>[^ ]*))? +HTTP\/1\.[01])|-?)$/';

    /**
     * Parser to use
     *
     * @var object
     */
    protected $parser = null;

    /**
     * Get parser used by this instance
     *
     * @return  object
     */
    public function getParser()
    {
        return $this->parser;
    }

    /**
     * Set parser to be used by this instance
     *
     * @param  object $parser
     * @return void
     */
    public function setParser($parser)
    {
        $this->parser = $parser;
    }

    /**
     * Request format to use
     *
     * @var string
     */
    protected $requestFormat = self::DEFAULT_REQUEST_FORMAT;

    /**
     * Get request format used by this instance
     *
     * @return  string
     */
    public function getRequestFormat()
    {
        return $this->requestFormat;
    }

    /**
     * Set request format to be used by this instance
     *
     * @param  string $requestFormat
     * @return void
     */
    public function setRequestFormat($requestFormat)
    {
        $this->requestFormat = $requestFormat;
    }

    /**
     * The entries of this log
     *
     * @var array
     */
    protected $entries;

    /**
     * Get the log items
     *
     * @return  array
     */
    public function getEntries()
    {
        if (isset($this->entries)) {
            return $this->entries;
        }

        $this->prepareEntries();

        return $this->entries;
    }

    /**
     * Read the entries from the log resource to the internal items array
     *
     */
    protected function prepareEntries()
    {
        $log = $this->read();

        $parser = $this->getParser();

        static $requestInfoDefaults = array (
            'method' => '',
            'protocol' => '',
            'domain' => '',
            'port' => '',
            'path' => '',
            'querystring' => '',
        );

        $this->entries = array();
        $token = "\r\n";
        $line = strtok($log, $token);
        while ($line !== false) {
            $entry = $parser->parse($line);
            if (preg_match($this->getRequestFormat(), $entry->request, $matches)) {
                $assocMatches = array_intersect_key($matches, $requestInfoDefaults);
                $requestInfo = (object) ($assocMatches + $requestInfoDefaults);
                $entry->requestInfo = $requestInfo;
            }
            $this->entries[] = $entry;
            $line = strtok($token);
        }
    }
}
