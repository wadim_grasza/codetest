<?php

namespace Wadim\CodeTestBundle\Data;

/**
 *
 */
class VarnishLogAggregator {

    const DEFAULT_RESULTS_COUNT = 5;

    /**
     * Varnish log to aggregate
     *
     * @var object
     */
    protected $varnishLog;

    /**
     * Number of results
     *
     * @var integer
     */
    protected $resultsCount;

    /**
     * Counted aggregates
     *
     * @var array
     */
    protected $aggregates;

    /* 
     * Constructor
     *
     * @param  VarnishLog $varnishLog
     * @param  int $resultsCount
     */
    public function __construct(VarnishLog $varnishLog, $resultsCount = self::DEFAULT_RESULTS_COUNT)
    {
        $this->varnishLog = $varnishLog;
        $this->resultsCount = $resultsCount;
    }

    /**
     * Get the aggregates
     *
     * @return  array
     */
    public function getAggregates()
    {
        if (!isset($this->aggregates)) {
            $this->prepareAggregates();
        }
        return $this->aggregates;
    }

    protected function prepareAggregates()
    {
        $entries = $this->varnishLog->getEntries();

        $hosts = array();
        $files = array ();
        foreach ($entries as $entry) {

            $requestInfo = $entry->requestInfo;

            if (!empty($requestInfo->domain)) {
                if (!isset($hosts[$requestInfo->domain])) {
                    $hosts[$requestInfo->domain] = 0;
                }
                $hosts[$requestInfo->domain] += 1;
            }

            $file = '';
            if (!empty($requestInfo->protocol)) {
                $file .= $requestInfo->protocol.'://';
            }
            $file .= $requestInfo->domain.$requestInfo->path;
            if (!empty($requestInfo->querystring)) {
                $file .= '?'.$requestInfo->querystring;
            }
            if (!isset($files[$file])) {
                $files[$file] = 0;
            }
            $files[$file] += 1;
        }

        arsort($hosts);
        arsort($files);

        $slicedHosts = array_slice($hosts, 0, $this->resultsCount);
        $slicedFiles = array_slice($files, 0, $this->resultsCount);

        $aggregateHosts = array();
        $aggregateFiles = array();

        foreach ($slicedHosts as $name => $visits) {
            $aggregateHosts[] = array ('name' => $name, 'visits' => $visits);
        }
        foreach ($slicedFiles as $name => $downloads) {
            $aggregateFiles[] = array ('name' => $name, 'downloads' => $downloads);
        }

        $this->aggregates = array();
        $this->aggregates['hosts'] = $aggregateHosts;
        $this->aggregates['files'] = $aggregateFiles;
    }
}
