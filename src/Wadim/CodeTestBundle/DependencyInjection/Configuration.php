<?php

namespace Wadim\CodeTestBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('wadim_code_test');

        /*
        $rootNode->children()
            ->arrayNode('files')
            ->isRequired()
                ->children()
                    ->scalarNode('log')
                        ->isRequired()
                        ->cannotBeEmpty()
                    ->end()
                    ->scalarNode('rss')
                        ->isRequired()
                        ->cannotBeEmpty()
                    ->end()
                    ->scalarNode('json')
                        ->isRequired()
                        ->cannotBeEmpty()
                    ->end()
                ->end()
            ->end()
        ->end();
        */

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
